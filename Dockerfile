FROM zoobab/arduino-cli

ADD cli-config.yml /usr/bin/.cli-config.yml

ADD src /src
RUN arduino-cli core update-index && \
    arduino-cli core install CubeCell:CubeCell  && \
    arduino-cli lib install CayenneLPP && \
    arduino-cli lib install ArduinoJson 
ADD platform.txt /root/.arduino15/packages/CubeCell/hardware/CubeCell/1.1.0/platform.txt     
ENV USER root
RUN arduino-cli compile --fqbn  CubeCell:CubeCell:CubeCell-Board /src


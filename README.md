Arduino-cli for CubeCell AB01
==============================================================

## Run

```bash
docker run -it --rm -v ${PWD}/src:/src -v ${PWD}/out:/tmp --name cubecell songritk/arduino-cubecell
```